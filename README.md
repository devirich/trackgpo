# TrackGpo

TrackGpo is a PowerShell module that tracks changes to Group Policy Objects (GPOs) in an Active Directory environment.

## Description
This module enables SysAdmins to easily store snapshots of their environment's group policy state into a repository. It can use a git repo or it can use just a standard folder. It can alert you to any changes made, or it can just be a backup of your GPO store. TrackGpo has sane defaults, but much of its behavior can be customized to fit your needs. The bahavior that can be customized includes things like whether or not to use a git repo, remove old versions of a GPO, remove GPOs from the folder after deletion, etc.

TrackGpo is mostly a "set it and forget it" package where you can take a few minutes to get it setup and then after that, you likely don't have to do much of anything with it again.

### What TrackGpo can be Used For
Here are some scenarios that TrackGpo can really help with:

1. Create a ticket anytime a GPO is added to your domain, changed, or removed.
1. Easily restore any GPO to a prior version.
1. Easily restore any GPO after deletion.
1. Create a 1:1 live backup of all GPO policies with full Git versioning.

### Extensibility
TrackGpo was written be extended. It has two external functions that it will search your system for and call when appropriate. These two functions are for when there is a GroupPolicy add/change/delete or when the module has an error. You can package your own module that contains these functions, or just have the functions in your environment. See the ##Getting Started/Extensibility section for more details.

## Requirements
You MUST have Git available on the machine that will run this module. TrackGpo uses the `git diff` command to compare files. It also uses Git to maintain a repository of all changes, if you don't disable that feature.

You also must have the GroupPolicy module installed (part of RSAT tools) in order to run commands like `Get-Gpo` and `Backup-Gpo`.

## Getting Started
Getting started with TrackGpo can be done in just a few minutes, but to get the most use out of it, you should consider spending some more time to extend its inbuilt capabilities.

### Security/Best practices
Before you begin installing the module, please review this section to understand the security implications and requirements for effective use of TrackGpo:

1. This module will back up your Group Policy Store to a folder. Be sure that the account that runs this script has the appropriate rights to read all group policies.
1. Group Policy Objects can have sensitive information in them. Be sure to configure folder security permissions on the repo and the working directory so that accounts with no business reading them do not have access (especially Domain Users, Authenticated Users, Domain Computers, and Everyone).

Because these are impossible for a module to predict and auto configure, YOU as the administrator are in charge of making sure to perform these steps to protect your environment. Ok, head on down to Installation now!

### Installation
If you have the [PowerShellGet](https://msdn.microsoft.com/powershell/gallery/readme) module installed you can enter the following command:

    Install-Module TrackGpo

Alternatively, you can download the ZIP file of the latest version from the [Releases](https://gitlab.com/devirich/trackgpo/-/releases) page and place it into one of your modules paths.

### Extensibility
If you want your own code to get run whenever there is a GroupPolicy change or error with running this module, you'll need to make a function available for these commands:

* New-TrackGpoTicket_External
* New-TrackGpoError_External

I've created a demo module that provides a sample of each of these functions. Its called [TrackGpo_Builtin](https://www.powershellgallery.com/packages/TrackGpo_BuiltIn). If you don't want to write a full module and instead just create a function to overload these commands, be sure either to not install TrackGpo_Builtin or make sure that your function gets imported to your session AFTER all modules that provide those commands. Failing to do so means that your function will get overwritten and removed from the session until imported again.

I've also created a module that will send you a SMTP message anytime there is action taken. Check it out, its called [TrackGpo_Smtp](https://www.powershellgallery.com/packages/TrackGpo_Smtp). Use Get-Help or look at its [project site](https://gitlab.com/devirich/trackgpo_smtp) to see how to use it.

If you create a nice module to use your helpdesk ticket module/API, please let me know and I'll mention it here so that others know that its available! Should be pretty easy to write for an API if I see lots of requests for a specific helpdesk API. 

## Usage
TrackGpo exposes only one cmdlet: Invoke-GpoTracking. Examples here are also found in the command help (accessible from `Get-Help Invoke-GpoTracking`).

By default, Invoke-GpoTracking will NOT create ANY folders. Nor will it initialize a Git repository. This is avoid the posibility of accidentally spreading GroupPolicy to unexpected locations. I would suggest manually running the cmdlet once as an "initial run" and then automating future runs that use the stricter settings. This helps to prevent transient issues like file server down, domain in weird state, etc. from causing you a lot of extra work when you may already have issues going on.

### Cmdlet usage
Here are some examples pulled from the help content:

    .EXAMPLE
	PS> Invoke-GpoTracking -GpoRepo GpoStore -WorkingDir GpoStore_working -Initialize -WhatIf

	What if: Performing the operation "Create Directory" on target "Destination: C:\Protected\GpoStore".
	What if: Performing the operation "Create Directory" on target "Destination: C:\Protected\GpoStore_working".
	What if: Performing the operation "Push-Location" on target "C:\Protected\GpoStore".
	What if: Performing the operation "Remove-Item" on target "C:\Protected\GpoStore_working\*".
	What if: Performing the operation "git `reset --hard`" on target "C:\Protected\GpoStore".
	What if: Performing the operation "Compare GroupPolicy to repo" on target "C:\Protected\GpoStore".
	What if: Back up all the GPOs in the domain.test domain to the following location: C:\Protected\GpoStore_working. (Backup-GPO)
	What if: Performing the operation "Set-Content" on target "$($_.BackupDirectory)\{$($_.ID)}\$($_.GpoId).summary (Foreach-Object)".
	What if: Performing the operation "Compare GPO GUIDs and reconcile changes" on target "`ls $GpoRepo\*\*.summary` and `ls $WorkingDir\*\*.summary`".
	What if: Performing the operation "Process any new GPO objects." on target "$WorkingDir\<FOLDER>".
	What if: Performing the operation "Process any updated GPO objects." on target "$WorkingDir\<FOLDER(S)>".
	What if: Performing the operation "Process any removed GPO objects." on target "$GpoRepo\<FOLDERS>".
	What if: Performing the operation "Remove-Item" on target "C:\Protected\GpoStore_working\*".
	What if: Performing the operation "git `reset --hard`" on target "C:\Protected\GpoStore".

	Enabling -Initialize to automatically create folders and using -WhatIf to get a view of what actions will be taken and where. 
	Confirm that these actions and paths are what you expect.
	Remove -WhatIf and let the script run!

	.EXAMPLE
	PS> Invoke-GpoTracking -GpoRepo GpoStore -WorkingDir GpoStore_working

	Use this for subsequent runs if you're ok with the defaults.

	.EXAMPLE
	PS> Invoke-GpoTracking -GpoRepo GpoStore -WorkingDir GpoStore_working -RemoveOldPolicyVersions -RemoveDeletedPolicies -DisableGitRepo -GpoChangeContext 0 -SkipCommonChanges

	This example will: Disable git functionality and make the repo folder a 1:1 match of live group policies in the domain.
	It also will set the context around each GPO diff to 0 lines above and below and skip showing the GPO revision number and modified date.



## Support
If you have encountered a bug, found gaps in the documentation that make it hard to understand what to do, or have a feature to suggest, please open an issue on this repo.

## Roadmap
If you have other ideas to suggest, please open an issue and tag it as a feature request.

Future enhancements that I'm watching out for:
* This module is built around a single domain in a single forest where you don't need to customize any of the GroupPolicy commands. A future release should enable more flexibility in this regard.
* Ability to mark a GPO as "in progress" so that each change doesn't lead to an update notification.
* Create a external cmdlet module that will email to SMTP server.
* Make -SkipCommonChanges also reduce the diff shortstats.
* Maybe Add a switch to use HTML instead of reduced summary report.
* Maybe roll an alternative to `git diff`. Unlikely though unless LOTS of people can't Git.

## Building the Module
If you would like to build the module yourself, clone this repo, install all modules listed at the top in TrackGpo.build.ps1. Finally, open the folder and run:

    Invoke-Build

Typically, you will want to then remove any instances of TrackGpo that are loaded in your session (if there are any modules to remove) and then import your newly build version of the module:

    Remove-Module TrackGpo
    Import-Module .\TrackGpo\TrackGpo.psd1


## Contributing
If you would like to contribute to this module, you'll need to open a Merge Request (known as a Pull Request/PR for non-gitlab users) for your feature to be considered.

## Acknowledgment
I'm always grateful to my wife who lets me dink around on computers all the time.

## License
This is licensed with a MIT license so that you can do what you want with the software and not have to worry about legal repercussions.
