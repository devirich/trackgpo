function New-TrackGpoTicket {
    param(
        [ValidateSet("Add", "Remove", "Change")]
        [parameter(Mandatory)]$Type,
        [parameter(Mandatory)]$GpoInfo,
        $Diff,
        $Stats
    )
    $Splat = @{
        Type    = $Type
        GpoInfo = $GpoInfo
    }
    if ($Diff) { $Splat.Add("Diff", $Diff) }
    if ($Stats) { $Splat.Add("Stats", $Stats) }

    $CommandName = $MyInvocation.InvocationName + "_External"
    if (Get-Command $CommandName -ea silent) {
        [string]$Message = & $CommandName @Splat
        $Message
    }
}
