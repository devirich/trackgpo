function Get-TrackGpoDeltaPercent {
    <#
    .SYNOPSIS
    Returns a percentage as 0-100 of how many GPOs are not common between the domain and a list of GPO ids

    .PARAMETER GpoRepo_LatestGpos
    Array of GPOs that was the last current snapshot of the domain
    #>
    [CmdletBinding()]
    [OutputType([int])]
    param (
        [Parameter()]
        $GpoRepo_LatestGpos
    )

    # Need a baseline of all previous GPOs in order to track possible failres

    if ($DomainGpos = Get-GPO -All) {
        # This check is placed inside the Get-GPO block to ensure that Get-GPO works even when there
        # are no existing GPO's getting tracked.
        if ($GpoRepo_LatestGpos) {
            Write-Verbose "Comparing $($DomainGpos.ID.Count) domain GPOs to $($GpoRepo_LatestGpos.BaseName.count) Repo GPOs"
            $Same,$Diff = (Compare-Object $DomainGpos.ID $GpoRepo_LatestGpos.BaseName -IncludeEqual).Where({$_.SideIndicator -eq "=="},"Split")
            $Diff.Count / ($Same.Count + $Diff.Count) * 100
        }
        else {
            # When there are no existing GPOs getting tracked, everything is changed. Return 100%
            Write-Verbose "No repo GPOs. 100% changed!"
            100
        }
    }
    else {
        throw "Could not get domain Group Policy Objects (GPOs). Please fix this issue. Permissions?"
    }
}
