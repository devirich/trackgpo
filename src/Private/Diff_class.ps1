class Diff {
    [string]$Header
    [string[]]$ExtendedHeaders
    [string]$From
    [string]$To
    [array[]]$Hunk

    Diff() {}
    Diff([string[]]$String) {
        # Need to ensure that whether you input an array of strings, or a string with multiple lines, or a combo,
        # that it ends up as an array of strings in a queue collection:
        $In = $String -join "`n"
        [System.Collections.Generic.Queue[string]]$Q = $In -split "`n"

        # Need to make sure that Q is properly populated. If so, convert to our object!
        if ($Q.Count -and $Q.Peek() -match "^diff") {
            $this.Header = $Q.Dequeue()
            $this.ExtendedHeaders = while ($Q.Peek() -notmatch "^---") {
                $Q.Dequeue()
            }
            $this.From = $Q.Dequeue()
            $this.To = $Q.Dequeue()
            $this.Hunk = $Q -join "`n" -split "(?m)`n(?=^@@)"
        }
    }

    [string[]] ToString() {
        return $this.ToString($null, $null, $null)
    }
    [string[]] ToString([string]$ExcludeHunkPattern) {
        return $this.ToString("-", $ExcludeHunkPattern, 2)
    }
    [string[]] ToString([string]$Modifier, [string]$HunkPattern, [int]$SearchScope) {
        [string[]]$Out = @()
        # SearchScope is used with -SkipCommonChanges to search in the first couple lines for the Computer or User revisions fields.
        # This feels a bit like a hack. Cause it is.
        switch ($Modifier) {
            "+" { $out += $this.Hunk | Select-Object -First $SearchScope | Where-Object { $_ -match $HunkPattern } }
            "-" { $out += $this.Hunk | Select-Object -First $SearchScope | Where-Object { $_ -notmatch $HunkPattern } }
            default { $out += $this.Hunk | Select-Object -First $SearchScope }
        }
        $out += $this.Hunk | Select-Object -Skip $SearchScope

        return $out | ForEach-Object { $_ -split "`n" }
    }
}
