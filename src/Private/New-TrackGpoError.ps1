function New-TrackGpoError {
    param(
        [parameter(Mandatory)]
        [String]$Message
    )
    $Splat = @{
        Message = $Message
    }
    $CommandName = $MyInvocation.InvocationName + "_External"
    if (Get-Command $CommandName -ea silent) {
        [string]$Message = & $CommandName @Splat
        $Message
    }
}
