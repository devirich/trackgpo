function ConvertFrom-Diff {
    [CmdletBinding()]
    param (
        [string[]]$In
    )
    $String = $In -join "`n"
    $Sections = $String -replace "`r" -split "(?m)`n(?=^diff)"
    $Sections | ForEach-Object { [Diff]::new($_) }
}
