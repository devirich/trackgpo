function Remove-HtmlContent {
    param([System.String[]] $html)
    # Adapted from: http://winstonfassett.com/blog/2010/09/21/html-to-text-conversion-in-powershell/
    # This function makes use of the single line (?s) regex modifier to make . apply to newlines
    # This function makes use of the multiline (?m) regex modifier to make ^|$ apply to newlines

    # Want to preserve line breaks for pretty formatting later, but need a single string with only newlines:
    $html = $html -replace "`r" -join "`n"

    # remove invisible content
    @('head', 'script', 'style', 'object', 'embed', 'applet', 'noframes', 'noscript', 'noembed') | ForEach-Object {
        $html = $html -replace "(?ms)<$_[^>]*?>.*?^</$_>", ""
    }
    # write-verbose "removed invisible blocks: `n`n$html`n"

    # Condense extra whitespace
    $html = $html -replace "( )+", " "
    # write-verbose "condensed whitespace: `n`n$html`n"
    # Remove the window styles
    $html = $html -replace '(?ms)<div id="explainText_windowStyles.*?</div>'

    # Add line breaks
    @('div', 'p', 'blockquote', 'h[1-9]', 'tr') | ForEach-Object { $html = $html -replace "(?ms)</?$_[^>]*?>.*?</$_>", ("`n" + '$0' ) }
    # Add line breaks for self-closing tags
    @('div', 'p', 'blockquote', 'h[1-9]', 'br') | ForEach-Object { $html = $html -replace "(?ms)<$_[^>]*?/>", ('$0' + "`n") }
    # write-verbose "added line breaks: `n`n$html`n"

    # table cells deserve a tab after them
    $html = $html -replace "</td>|</th>", " `t"

    #strip tags
    $html = $html -replace "<[^>]*?>", ""
    # write-verbose "removed tags: `n`n$html`n"

    # replace common entities
    @(
        @("&nbsp;", " "),
        @("&amp;bull;", " * "),
        @("&amp;lsaquo;", "<"),
        @("&amp;rsaquo;", ">"),
        @("&amp;(rsquo|lsquo|#39|#039);", "'"),
        @("&#0?39;", "'"),
        @("&amp;(quot|ldquo|rdquo);", '"'),
        @("&amp;trade;", "(tm)"),
        @("&amp;frasl;", "/"),
        @("&amp;(quot|#34|#034|#x22);", '"'),
        @('&amp;(amp|#38|#038|#x26);', "&amp;"),
        @("&amp;(lt|#60|#060|#x3c);", "<"),
        @("&amp;(gt|#62|#062|#x3e);", ">"),
        @('&amp;(copy|#169);', "(c)"),
        @("&amp;(reg|#174);", "(r)"),
        @("&amp;nbsp;", " "),
        @("&amp;(.{2,6});", "")
    ) | ForEach-Object { $html = $html -replace $_[0], $_[1] }
    # write-verbose "replaced entities: `n`n$html`n"

    # Extra lines should get condensed
    $html = $html -replace "`n+", "`n"
    $html -split "`n"

}
