function Get-GpoInfo ([string]$FilePath) {
    $GpoContents = Get-Content $FilePath
    $GpoInfo = [ordered]@{
        Title           = [regex]::Match($GpoContents, '(?<=<title>).*?(?=</title>)').Value
        Created         = [regex]::Match($GpoContents, '(?<="row">Created</td><td>).*?(?=</td></tr>)').Value
        Modified        = [regex]::Match($GpoContents, '(?<="row">Modified</td><td>).*?(?=</td></tr>)').Value
        GUID            = [regex]::Match($GpoContents, '(?<="row">Unique ID</td><td>).*?(?=</td></tr>)').Value
        'GPO Status'    = [regex]::Match($GpoContents, '(?<="row">GPO Status</td><td>).*?(?=</td></tr>)').Value
        'Enabled Links' = [regex]::Matches($GpoContents, '(?<=<td>Enabled</td><td>)feb.com/.*?(?=</td>)').Value -join "`n"
    }
    $GpoInfo
}
